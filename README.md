Cloud privée MedicArche
-----------------------

Le script permet d'installer la machine virtual hote avec l'OS Ubuntu sur virtualbox en utilisant vagrant qui est un logiciel open-source de création et de configuration des environnements de développement virtuel. 

Le fichier Vagrantfile contient un script qui provisionne l'installation de la platform cloud privée Microstack. 
Après l'installation de Microstack, le script d'installation des vms et des applications prend le relai.

